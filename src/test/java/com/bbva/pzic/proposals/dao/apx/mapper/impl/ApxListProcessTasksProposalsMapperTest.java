package com.bbva.pzic.proposals.dao.apx.mapper.impl;

import com.bbva.pzic.proposals.EntityStubs;
import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTask;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

import static com.bbva.pzic.proposals.EntityStubs.*;
/**
 * Created on 22/04/2021.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ApxListProcessTasksProposalsMapperTest {

    @InjectMocks
    private ApxListProcessTasksProposalsMapper mapper;

    @Test
    public void mapInFullTest() throws IOException {
        DTOInputListProcessTasksProposals input = EntityStubs.getInstance().getDtoInputListProcessTasksProposals();
        PeticionTransaccionPpcutge1_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getTaskid());
        assertNotNull(result.getBusinessprocessid());

        assertEquals(TASK_ID,result.getTaskid());
        assertEquals(BUSINESS_PROCESS_ID,result.getBusinessprocessid());
    }

    @Test
    public void mapInEmptyTest() {
        PeticionTransaccionPpcutge1_1 result = mapper.mapIn(new DTOInputListProcessTasksProposals());
        assertNotNull(result);
        assertNull(result.getTaskid());
        assertNull(result.getBusinessprocessid());
    }

    @Test
    public void mapOutFullTest() throws IOException{
        RespuestaTransaccionPpcutge1_1 outPut = EntityStubs.getInstance().getRespuestaTransaccionPpcutge1_1();

        List<ProcessTask> result = mapper.mapOut(outPut);

        assertNotNull(result);
        assertNotNull(result.get(0));
        assertNotNull(result.get(0).getTaskId());
        assertNotNull(result.get(0).getBusinessProcessId());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getStatus().getId());
        assertNotNull(result.get(0).getStatus().getDescription());
        assertEquals(TASK_ID,result.get(0).getTaskId());
        assertEquals(BUSINESS_PROCESS_ID,result.get(0).getBusinessProcessId());
        assertEquals(PROCESS_STATUS_ID,result.get(0).getStatus().getId());
        assertEquals(PROCESS_STATUS_DESCRIPTION,result.get(0).getStatus().getDescription());

    }
    @Test
    public void mapOutEmptyTest() {
        List<ProcessTask> result = mapper.mapOut(new RespuestaTransaccionPpcutge1_1());
        assertNotNull(result);
    }
}