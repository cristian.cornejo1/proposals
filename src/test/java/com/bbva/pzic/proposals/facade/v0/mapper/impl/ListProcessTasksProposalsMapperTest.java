package com.bbva.pzic.proposals.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTask;
import com.bbva.pzic.proposals.facade.v0.mapper.IListProcessTasksProposalsMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.bbva.pzic.proposals.EntityStubs.*;
import static org.junit.Assert.*;
/**
 * Created on 22/04/2021.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ListProcessTasksProposalsMapperTest{


    private IListProcessTasksProposalsMapper mapper;
    @Before
    public void setUp() {
        mapper = new ListProcessTasksProposalsMapper();
    }

    @Test
    public void mapInFullTest(){

        DTOInputListProcessTasksProposals result = mapper.mapIn(BUSINESS_PROCESS_ID, TASK_ID);
        assertNotNull(result);
        assertNotNull(result.getBusinessProcessId());
        assertNotNull(result.getTaskId());

        assertEquals(BUSINESS_PROCESS_ID,result.getBusinessProcessId());
        assertEquals(TASK_ID,result.getTaskId());
    }

    @Test
    public void mapOutFullTest(){
        ServiceResponse<List<ProcessTask>> result = mapper.mapOut(new ArrayList<>());
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest(){
        ServiceResponse<List<ProcessTask>> response = mapper.mapOut(null);
        assertNull(response);
    }
}