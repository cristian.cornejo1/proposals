package com.bbva.pzic.proposals.business.dto;


/**
 * Created on 28/12/2017.
 *
 * @author Entelgy
 */
public class DTOIntTerm {

    private String frequency;
    private Integer value;

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}