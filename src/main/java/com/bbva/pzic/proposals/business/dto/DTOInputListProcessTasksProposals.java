package com.bbva.pzic.proposals.business.dto;

import javax.validation.constraints.Size;

public class DTOInputListProcessTasksProposals {

    @Size(max = 40, groups = ValidationGroup.ListProcessTasksProposals.class)
    private String businessProcessId;

    @Size(max = 40, groups = ValidationGroup.ListProcessTasksProposals.class)
    private String taskId;

    public String getBusinessProcessId() {
        return businessProcessId;
    }

    public void setBusinessProcessId(String businessProcessId) {
        this.businessProcessId = businessProcessId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
