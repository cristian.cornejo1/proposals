package com.bbva.pzic.proposals.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTask;

import java.util.List;

public interface IListProcessTasksProposalsMapper {

    DTOInputListProcessTasksProposals mapIn(String businessProcessId, String taskId);

    ServiceResponse<List<ProcessTask>> mapOut(List<ProcessTask> processTasks);
}
