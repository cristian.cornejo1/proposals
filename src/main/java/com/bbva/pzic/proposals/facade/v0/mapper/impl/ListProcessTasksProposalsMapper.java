package com.bbva.pzic.proposals.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTask;
import com.bbva.pzic.proposals.facade.v0.mapper.IListProcessTasksProposalsMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ListProcessTasksProposalsMapper implements IListProcessTasksProposalsMapper {

    private static final Log LOG = LogFactory.getLog(ListProcessTasksProposalsMapper.class);

    @Override
    public DTOInputListProcessTasksProposals mapIn(final String businessProcessId,final String taskId) {
        LOG.info("... called method ListProcessTasksProposalsMapper.mapIn ...");
        DTOInputListProcessTasksProposals dtoInputListProcessTasksProposals = new DTOInputListProcessTasksProposals();
        dtoInputListProcessTasksProposals.setBusinessProcessId(businessProcessId);
        dtoInputListProcessTasksProposals.setTaskId(taskId);
        return dtoInputListProcessTasksProposals;
    }

    @Override
    public ServiceResponse<List<ProcessTask>> mapOut(final List<ProcessTask> processTasks) {
        LOG.info("... called method ListProcessTasksProposalsMapper.mapOut ...");
        if(processTasks == null){
            return null;
        }
        return ServiceResponse.data(processTasks).pagination(null).build();
    }
}
