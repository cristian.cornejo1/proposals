package com.bbva.pzic.proposals.facade;

/**
 * @author Entelgy
 */
public final class RegistryIds {

    public static final String SMC_REGISTRY_ID_OF_CREATE_QUESTIONNAIRES_VALIDATE_ACCESS = "SMCPE1920120";
    public static final String SMC_REGISTRY_ID_OF_LIST_PROCESS_TASKS_PROPOSALS = "SMGG20203823";

    private RegistryIds() {
    }
}
