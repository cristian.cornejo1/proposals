package com.bbva.pzic.proposals.dao.apx.mapper;

import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTask;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import java.util.List;

public interface IApxListProcessTasksProposalsMapper {

    PeticionTransaccionPpcutge1_1 mapIn(DTOInputListProcessTasksProposals dtoInt);

    List<ProcessTask> mapOut(RespuestaTransaccionPpcutge1_1 dotOut);

}
