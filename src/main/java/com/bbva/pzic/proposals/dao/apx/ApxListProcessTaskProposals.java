package com.bbva.pzic.proposals.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.dao.apx.mapper.IApxListProcessTasksProposalsMapper;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created on 22/04/2021.
 *
 * @author Entelgy
 */
@Component
public class ApxListProcessTaskProposals {

    @Autowired
    private IApxListProcessTasksProposalsMapper mapper;

    @Resource(name = "transaccionPpcutge1_1")
    private transient InvocadorTransaccion<PeticionTransaccionPpcutge1_1, RespuestaTransaccionPpcutge1_1> transaccion;

    public List<ProcessTask> perform(final DTOInputListProcessTasksProposals dtoInput){

        PeticionTransaccionPpcutge1_1 request = mapper.mapIn(dtoInput);
        RespuestaTransaccionPpcutge1_1 response = transaccion.invocar(request);
        return  mapper.mapOut(response);
    }
}
