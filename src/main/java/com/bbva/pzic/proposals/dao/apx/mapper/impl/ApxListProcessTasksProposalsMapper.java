package com.bbva.pzic.proposals.dao.apx.mapper.impl;

import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.dao.apx.mapper.IApxListProcessTasksProposalsMapper;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTask;
import com.bbva.pzic.proposals.facade.v0.dto.Status;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.bbva.pzic.proposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;

import java.util.ArrayList;
import java.util.List;

@Component
public class ApxListProcessTasksProposalsMapper implements IApxListProcessTasksProposalsMapper {

    private static final Log LOG = LogFactory.getLog(ApxListProcessTasksProposalsMapper.class);

    @Override
    public PeticionTransaccionPpcutge1_1 mapIn(final DTOInputListProcessTasksProposals dtoInt) {
        LOG.info("... called method ApxListProcessTasksProposalsMapper.mapIn ...");
        PeticionTransaccionPpcutge1_1 peticion = new PeticionTransaccionPpcutge1_1();
        peticion.setBusinessprocessid(dtoInt.getBusinessProcessId());
        peticion.setTaskid(dtoInt.getTaskId());
        return peticion;
    }

    @Override
    public List<ProcessTask> mapOut(final RespuestaTransaccionPpcutge1_1 dotOut) {
        LOG.info("... called method ApxListProcessTasksProposalsMapper.mapOut ...");
        if (dotOut == null) {
            return null;
        }
        ProcessTask processTask = new ProcessTask();
        processTask.setBusinessProcessId(dotOut.getCampo_1_businessprocessid());
        processTask.setTaskId(dotOut.getCampo_2_taskid());
        processTask.setStatus(mapOutStatus(dotOut.getStatus()));

        List<ProcessTask> processTasks = new ArrayList<>();
        processTasks.add(processTask);
        return processTasks;
    }

    private Status mapOutStatus(com.bbva.pzic.proposals.dao.model.ppcutge1_1.Status status) {
        if(status == null){
            return null;
        }
        Status status1 = new Status();
        status1.setDescription(status.getDescription());
        status1.setId(status.getId());
        return status1;
    }

}
