package com.bbva.pzic.proposals.dao.model.listproposals;

import java.math.BigDecimal;

/**
 * Created on 31/01/2017.
 *
 * @author Entelgy
 */
public class FormatProposal {

    private String campanha;
    private String codConfigProducto;
    private String codFlujoOpe;
    private Integer codPlazo;
    private String codInterno;
    private String codProducto;
    private String codSubProducto;
    private String codigoCentral;
    private String desConfigProducto;
    private String desProducto;
    private String desSubProducto;
    private String divisa;
    private String familiaProducto;
    private String flujoOperativo;
    private String idPropuesta;
    private BigDecimal rangoMax;
    private BigDecimal rangoMin;
    private String stRiesgo;
    private BigDecimal tasaMax;
    private BigDecimal tasaMin;
    private String tipplazo;
    private String valBin;
    private BigDecimal valCuotaAjust;
    private BigDecimal valCuotaContrato;
    private BigDecimal valCuotaReal;
    private BigDecimal valLimiteAjust;
    private BigDecimal valLimiteContrato;
    private BigDecimal valLimiteReal;
    private BigDecimal valTasa;
    private String vdomiciliaria;
    private String vlaboral;

    public String getCampanha() {
        return campanha;
    }

    public void setCampanha(String campanha) {
        this.campanha = campanha;
    }

    public String getCodConfigProducto() {
        return codConfigProducto;
    }

    public void setCodConfigProducto(String codConfigProducto) {
        this.codConfigProducto = codConfigProducto;
    }

    public String getCodFlujoOpe() {
        return codFlujoOpe;
    }

    public void setCodFlujoOpe(String codFlujoOpe) {
        this.codFlujoOpe = codFlujoOpe;
    }

    public Integer getCodPlazo() {
        return codPlazo;
    }

    public void setCodPlazo(Integer codPlazo) {
        this.codPlazo = codPlazo;
    }

    public String getCodInterno() {
        return codInterno;
    }

    public void setCodInterno(String codInterno) {
        this.codInterno = codInterno;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public String getCodSubProducto() {
        return codSubProducto;
    }

    public void setCodSubProducto(String codSubProducto) {
        this.codSubProducto = codSubProducto;
    }

    public String getCodigoCentral() {
        return codigoCentral;
    }

    public void setCodigoCentral(String codigoCentral) {
        this.codigoCentral = codigoCentral;
    }

    public String getDesConfigProducto() {
        return desConfigProducto;
    }

    public void setDesConfigProducto(String desConfigProducto) {
        this.desConfigProducto = desConfigProducto;
    }

    public String getDesProducto() {
        return desProducto;
    }

    public void setDesProducto(String desProducto) {
        this.desProducto = desProducto;
    }

    public String getDesSubProducto() {
        return desSubProducto;
    }

    public void setDesSubProducto(String desSubProducto) {
        this.desSubProducto = desSubProducto;
    }

    public String getDivisa() {
        return divisa;
    }

    public void setDivisa(String divisa) {
        this.divisa = divisa;
    }

    public String getFamiliaProducto() {
        return familiaProducto;
    }

    public void setFamiliaProducto(String familiaProducto) {
        this.familiaProducto = familiaProducto;
    }

    public String getFlujoOperativo() {
        return flujoOperativo;
    }

    public void setFlujoOperativo(String flujoOperativo) {
        this.flujoOperativo = flujoOperativo;
    }

    public String getIdPropuesta() {
        return idPropuesta;
    }

    public void setIdPropuesta(String idPropuesta) {
        this.idPropuesta = idPropuesta;
    }

    public BigDecimal getRangoMax() {
        return rangoMax;
    }

    public void setRangoMax(BigDecimal rangoMax) {
        this.rangoMax = rangoMax;
    }

    public BigDecimal getRangoMin() {
        return rangoMin;
    }

    public void setRangoMin(BigDecimal rangoMin) {
        this.rangoMin = rangoMin;
    }

    public String getStRiesgo() {
        return stRiesgo;
    }

    public void setStRiesgo(String stRiesgo) {
        this.stRiesgo = stRiesgo;
    }

    public BigDecimal getTasaMax() {
        return tasaMax;
    }

    public void setTasaMax(BigDecimal tasaMax) {
        this.tasaMax = tasaMax;
    }

    public BigDecimal getTasaMin() {
        return tasaMin;
    }

    public void setTasaMin(BigDecimal tasaMin) {
        this.tasaMin = tasaMin;
    }

    public String getTipplazo() {
        return tipplazo;
    }

    public void setTipplazo(String tipplazo) {
        this.tipplazo = tipplazo;
    }

    public String getValBin() {
        return valBin;
    }

    public void setValBin(String valBin) {
        this.valBin = valBin;
    }

    public BigDecimal getValCuotaAjust() {
        return valCuotaAjust;
    }

    public void setValCuotaAjust(BigDecimal valCuotaAjust) {
        this.valCuotaAjust = valCuotaAjust;
    }

    public BigDecimal getValCuotaContrato() {
        return valCuotaContrato;
    }

    public void setValCuotaContrato(BigDecimal valCuotaContrato) {
        this.valCuotaContrato = valCuotaContrato;
    }

    public BigDecimal getValCuotaReal() {
        return valCuotaReal;
    }

    public void setValCuotaReal(BigDecimal valCuotaReal) {
        this.valCuotaReal = valCuotaReal;
    }

    public BigDecimal getValLimiteAjust() {
        return valLimiteAjust;
    }

    public void setValLimiteAjust(BigDecimal valLimiteAjust) {
        this.valLimiteAjust = valLimiteAjust;
    }

    public BigDecimal getValLimiteContrato() {
        return valLimiteContrato;
    }

    public void setValLimiteContrato(BigDecimal valLimiteContrato) {
        this.valLimiteContrato = valLimiteContrato;
    }

    public BigDecimal getValLimiteReal() {
        return valLimiteReal;
    }

    public void setValLimiteReal(BigDecimal valLimiteReal) {
        this.valLimiteReal = valLimiteReal;
    }

    public BigDecimal getValTasa() {
        return valTasa;
    }

    public void setValTasa(BigDecimal valTasa) {
        this.valTasa = valTasa;
    }

    public String getVdomiciliaria() {
        return vdomiciliaria;
    }

    public void setVdomiciliaria(String vdomiciliaria) {
        this.vdomiciliaria = vdomiciliaria;
    }

    public String getVlaboral() {
        return vlaboral;
    }

    public void setVlaboral(String vlaboral) {
        this.vlaboral = vlaboral;
    }
}
